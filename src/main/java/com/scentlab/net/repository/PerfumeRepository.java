package com.scentlab.net.repository;

import com.scentlab.net.entity.PerfumeFragrantica;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PerfumeRepository extends JpaRepository<PerfumeFragrantica,Long> {
    PerfumeFragrantica findByPerfumeFragranticaId(Long id);
    List<PerfumeFragrantica> findAll();
    List<PerfumeFragrantica> findAllByNameContaining( @Param("name") String name);
}
