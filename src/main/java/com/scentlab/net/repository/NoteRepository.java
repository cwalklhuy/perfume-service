package com.scentlab.net.repository;

import com.scentlab.net.entity.Note;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NoteRepository extends JpaRepository<Note,Long> {
    Note findByNoteId(int id);
    List<Note> findAllByNameContaining(@Param("name") String name);
}
