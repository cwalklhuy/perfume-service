package com.scentlab.net.repository;

import com.scentlab.net.entity.NotePerfume;
import com.scentlab.net.entity.NotePerfumePK;
import com.scentlab.net.entity.PerfumeFragrantica;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NotePerfumeRepository extends JpaRepository<NotePerfume, NotePerfumePK> {

    List<NotePerfume> findAllByPerfumeFragrantica(PerfumeFragrantica dto);

}
