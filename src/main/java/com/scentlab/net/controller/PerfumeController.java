package com.scentlab.net.controller;

import com.scentlab.net.entity.Note;
import com.scentlab.net.payload.request.FindRequest;
import com.scentlab.net.payload.request.PerfumeRequest;
import com.scentlab.net.payload.response.PerfumeJson;
import com.scentlab.net.payload.response.PerfumeNewest;
import com.scentlab.net.payload.response.PerfumeNote;
import com.scentlab.net.service.NotePerfumeService;
import com.scentlab.net.service.PerfumeService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/perfumes")
@CrossOrigin
public class PerfumeController {

    public final PerfumeService perfumeService;

    private final NotePerfumeService notePerfumeService;

    public PerfumeController(PerfumeService perfumeFraService, NotePerfumeService notePerfumeService) {
        this.perfumeService = perfumeFraService;
        this.notePerfumeService = notePerfumeService;
    }

    @PostMapping("/save")
    public String saveDB(@RequestBody PerfumeRequest perfumeRequest) {
        return notePerfumeService.save(perfumeRequest);

    }

    @GetMapping("/newest")
    public ResponseEntity<List<PerfumeNewest>> getPerfumeNewest(@RequestParam(defaultValue = "0") Integer page) {

        return perfumeService.getPerfumeNewest(page);
    }

    @PostMapping("/saveNote")
    public Note saveNote(@RequestBody Note note) {
        return notePerfumeService.saveNote(note);

    }

    @PutMapping("/update/{id}")
    public String updateDB(@PathVariable Long id, @RequestBody PerfumeRequest perfumeRequest) {
        return notePerfumeService.update(id, perfumeRequest);
    }

    @PostMapping("/name")
    public List<PerfumeJson> searchByName(@RequestBody FindRequest param) {
        System.out.println(param.getName());
        return perfumeService.findAllByName(param.getName());
    }

    @GetMapping("/getallNote")
    public List<Note> loadAllNote() {
        return notePerfumeService.listNote();
    }

    @GetMapping("/popular")
    public ResponseEntity<List<PerfumeNewest>> getPerfumePopular(@RequestParam(defaultValue = "0") Integer page) {
        return perfumeService.getPerfumePopular(page);
    }

    @GetMapping("/find-all-perfumes")
    public ResponseEntity<List<PerfumeNote>> getAll() {

        return perfumeService.getAll();
    }
    @GetMapping("/getall")
    public List<PerfumeJson> getAllPerfume() {

        return  perfumeService.listAll();
    }


    @DeleteMapping("/deleteNote/{id}")
    public void deleteNote(@PathVariable Long id) {

        notePerfumeService.deleteNote(id);
    }

    @PostMapping("/nameNote")
    public List<Note> searchByNote(@RequestBody FindRequest param) {
        System.out.println(param.getName());
        return notePerfumeService.findAllByName(param.getName());
    }
}
