package com.scentlab.net.payload.response;

import com.scentlab.net.entity.PerfumeFragrantica;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class PerfumeNewest {
    private Long perfumeId;
    private String name;
    private String image;
    private String dateCreated;

    public PerfumeNewest(PerfumeFragrantica perfumeFragrantica) {
        this.perfumeId = perfumeFragrantica.getPerfumeFragranticaId();
        this.name = perfumeFragrantica.getName();
        this.image = perfumeFragrantica.getImage();
        this.dateCreated = "2020-02-02";
    }
}
