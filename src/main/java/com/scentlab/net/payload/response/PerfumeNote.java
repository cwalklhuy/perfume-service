package com.scentlab.net.payload.response;

import com.scentlab.net.entity.Note;
import com.scentlab.net.entity.NotePerfume;
import com.scentlab.net.entity.PerfumeFragrantica;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PerfumeNote {
    private Long perfumeId;
    private String perfumeName;
    private List<Note> notes;

    public PerfumeNote(PerfumeFragrantica perfumeFragrantica, List<Note> notes) {
        this.perfumeId = perfumeFragrantica.getPerfumeFragranticaId();
        this.perfumeName = perfumeFragrantica.getName();
        this.notes = notes;
    }
}
