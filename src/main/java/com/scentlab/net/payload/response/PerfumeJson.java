package com.scentlab.net.payload.response;

import com.scentlab.net.entity.Note;
import lombok.*;

import java.util.List;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PerfumeJson {
    private Long id;
    private int gender;
    private String image;
    private String name;
    private String detailLink;
    private List<Note> listNote;

}
