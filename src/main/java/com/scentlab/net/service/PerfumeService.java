package com.scentlab.net.service;

import com.scentlab.net.entity.Note;
import com.scentlab.net.entity.NotePerfume;
import com.scentlab.net.entity.PerfumeFragrantica;
import com.scentlab.net.payload.response.PerfumeJson;
import com.scentlab.net.payload.response.PerfumeNewest;
import com.scentlab.net.payload.response.PerfumeNote;
import com.scentlab.net.repository.PerfumeRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PerfumeService {

    private final PerfumeRepository perfumeRepository;

    public PerfumeService(PerfumeRepository perfumeRepository) {
        this.perfumeRepository = perfumeRepository;
    }

    public ResponseEntity<List<PerfumeNewest>> getPerfumeNewest(Integer page) {
        Sort sort = Sort.by(Sort.Direction.ASC, "perfumeFragranticaId");
        return pageSort(page, sort);
    }
    public List<PerfumeJson> listAll() {

        List<PerfumeFragrantica> perfumeFragranticaList = perfumeRepository.findAll();
        List<PerfumeJson> listPerfume=new ArrayList<>();

//        for (PerfumeFragrantica item : perfumeFragranticaList) {
            for (int i = 0; i < 50; i++) {
                PerfumeFragrantica item = perfumeFragranticaList.get(i);
                try {
                    System.out.println(item.getPerfumeFragranticaId());
                    List<Note> listNote = item.getNotePerfumes().stream()
                            .map(NotePerfume::getNote)
                            .collect(Collectors.toList());

                    PerfumeJson perfumeJson = new PerfumeJson();
                    perfumeJson.setId(item.getPerfumeFragranticaId());
                    perfumeJson.setName(item.getName());
                    perfumeJson.setDetailLink(item.getDetailLink());
                    perfumeJson.setGender(item.getGender());
                    perfumeJson.setImage(item.getImage());
                    perfumeJson.setListNote(listNote);
                    listPerfume.add(perfumeJson);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


//        }
            return listPerfume;
        }



        public List<PerfumeJson> findAllByName(String name) {
        List<PerfumeFragrantica> perfumeFragranticaList = perfumeRepository.findAllByNameContaining(name);
        List<PerfumeJson> perfumeJsons = new ArrayList<>();
        perfumeFragranticaList.forEach(e -> {
            final List<Note> notes = e.getNotePerfumes().stream().map(NotePerfume::getNote).collect(Collectors.toList());
            perfumeJsons.add(PerfumeJson.builder()
                    .id(e.getPerfumeFragranticaId())
                    .gender(e.getGender())
                    .image(e.getImage())
                    .name(e.getName())
                    .detailLink(e.getDetailLink())
                    .listNote(notes)
                    .build());
        });
        
        return perfumeJsons;
    }

    public ResponseEntity<List<PerfumeNewest>> getPerfumePopular(Integer page) {
        Sort sort = Sort.by(Sort.Direction.DESC, "popular");
        return pageSort(page, sort);

    }

    private ResponseEntity<List<PerfumeNewest>> pageSort(Integer page, Sort sort) {
        Pageable firstPageWithTenElements = PageRequest.of(page, 20, sort);
        final List<PerfumeFragrantica> content = perfumeRepository.findAll(firstPageWithTenElements).getContent();
        final List<PerfumeNewest> perfumePopulars = content.stream()
                .map(PerfumeNewest::new).collect(Collectors.toList());

        return ResponseEntity.ok().body(perfumePopulars);
    }

    public ResponseEntity<List<PerfumeNote>> getAll() {
        Sort sort = Sort.by(Sort.Direction.DESC, "perfumeFragranticaId");
        Pageable firstPageWithTenElements = PageRequest.of(0, 1000, sort);
        final List<PerfumeFragrantica> fragranticas = perfumeRepository.findAll(firstPageWithTenElements).getContent();
        List<PerfumeNote> perfumeNotes = new ArrayList<>();
        fragranticas.forEach(e -> {
            final List<Note> notes = e.getNotePerfumes().stream()
                    .map(NotePerfume::getNote)
                    .collect(Collectors.toList());
            perfumeNotes.add(new PerfumeNote(e, notes));
        });
        System.out.println(perfumeNotes.size());
        return ResponseEntity.ok().body(perfumeNotes);
    }
}
