package com.scentlab.net.service;

import com.scentlab.net.entity.Note;
import com.scentlab.net.entity.NotePerfume;
import com.scentlab.net.entity.NotePerfumePK;
import com.scentlab.net.entity.PerfumeFragrantica;
import com.scentlab.net.payload.request.PerfumeRequest;
import com.scentlab.net.repository.NotePerfumeRepository;
import com.scentlab.net.repository.NoteRepository;
import com.scentlab.net.repository.PerfumeRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class NotePerfumeService  {

    private final NotePerfumeRepository notePerfumeRepository;
    private final NoteRepository noteRepository;
    private final PerfumeRepository perfumeRepository;

    public NotePerfumeService(NotePerfumeRepository notePerfumeRepository, NoteRepository noteRepository, PerfumeRepository perfumeRepository) {
        this.notePerfumeRepository = notePerfumeRepository;
        this.noteRepository = noteRepository;
        this.perfumeRepository = perfumeRepository;
    }

    public String save(PerfumeRequest perfumeRequest) {
        PerfumeFragrantica perfumeFragrantica = PerfumeFragrantica.builder()
                .name(perfumeRequest.getName())
                .gender(perfumeRequest.getGender())
                .image(perfumeRequest.getImage())
                .detailLink(perfumeRequest.getDetailLink())
                .build();

        PerfumeFragrantica perfumeFragrantica1 = perfumeRepository.save(perfumeFragrantica);

        System.out.println(perfumeFragrantica1.getPerfumeFragranticaId());
        List<Note> noteList = noteRepository.findAllById(perfumeRequest.getNoteId());
        List<NotePerfume> notePerfumes = noteList.stream().map(e -> NotePerfume.builder()
                .notePerfumePK(new NotePerfumePK(perfumeFragrantica1.getPerfumeFragranticaId(),e.getNoteId()))
                .note(e)
                .perfumeFragrantica(perfumeFragrantica1)
                .build()).collect(Collectors.toList());
        List<NotePerfume> list = notePerfumeRepository.saveAll(notePerfumes);
        return list.isEmpty() ? "fail" : "success";
    }

    public String update(Long id, PerfumeRequest perfumeRequest) {
        PerfumeFragrantica perfumeFragrantica = perfumeRepository.findByPerfumeFragranticaId(id);

        perfumeFragrantica.setName(perfumeRequest.getName());
        perfumeFragrantica.setGender(perfumeRequest.getGender());
        perfumeFragrantica.setImage(perfumeRequest.getImage());
        PerfumeFragrantica perfumeFragrantica1=perfumeRepository.save(perfumeFragrantica);
        List <NotePerfume> notePerfumeList = notePerfumeRepository.findAllByPerfumeFragrantica(perfumeFragrantica);
        notePerfumeRepository.deleteAll(notePerfumeList);

        List<Note> noteList = noteRepository.findAllById(perfumeRequest.getNoteId());
        List<NotePerfume> notePerfumes = noteList.stream().map(e -> NotePerfume.builder()
                .notePerfumePK(new NotePerfumePK(perfumeFragrantica1.getPerfumeFragranticaId(),e.getNoteId()))
                .note(e)
                .perfumeFragrantica(perfumeFragrantica1)
                .build()).collect(Collectors.toList());
        List<NotePerfume> list = notePerfumeRepository.saveAll(notePerfumes);

        return list.isEmpty() ? "fail" : "success";
    }

    public Note saveNote(Note note) {

        return noteRepository.save(note);
    }

    public List<Note> listNote() {
        return noteRepository.findAll();
    }

    public void deleteNote(Long id) {

         noteRepository.deleteById(id);
    }

    public List<Note> findAllByName(String name) {
        return noteRepository.findAllByNameContaining(name);
    }
}

