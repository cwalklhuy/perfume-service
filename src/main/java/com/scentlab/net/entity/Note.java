package com.scentlab.net.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Collection;
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "note")
public class Note {

    @Id
    @Column(name = "note_id")
    @GenericGenerator(name = "generator", strategy = "increment")
    @GeneratedValue(generator = "generator")
    private Long noteId;

    @Column(name = "note", nullable = false, length = 32)
    private String name;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE})
    @JoinColumn(name = "group_note_id", referencedColumnName = "group_note_id")
    private GroupNote groupNote;

    @JsonIgnore
    @OneToMany(mappedBy = "note", cascade = {CascadeType.MERGE}, fetch = FetchType.LAZY)
    private Collection<NotePerfume> notePerfumes;
}
