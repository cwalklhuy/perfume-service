package com.scentlab.net.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "group_note")
public class GroupNote {

    @Id
    @Column(name = "group_note_id")
    @GenericGenerator(name = "generator", strategy = "increment")
    @GeneratedValue(generator = "generator")
    private Integer groupNoteId;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "groupNote", fetch = FetchType.LAZY)
    private Collection<Note> notes;
}
